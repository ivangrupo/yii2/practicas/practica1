<?php

    use yii\helpers\Html;

?>

<div class="site-index">

    <div class="jumbotron">
        <h1><?= $libro->titulo?></h1>
        
        <p><?= $libro->mensaje?></p>
        
        <p><?= Html::img('@web/imgs/' . $libro->foto, ['alt' => 'Foto 1']) ?></p>
        
    </div>
</div>
